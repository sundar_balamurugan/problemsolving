const arr = [10, 13, 15, 20, 35, 42, 57];
const search = 42;

let first = 0;
let last = arr.length - 1;

function binarysearch(arr, search, first, last) {
    if (last >= first) {
        let mid = Math.floor((first + last) / 2)
        if (arr[mid] == search) {
            return mid;
        }
        else if (arr[mid] > search)
            return binarysearch(arr, search, first, mid - 1);
        else
            return binarysearch(arr, search, mid + 1, last);
    }
}

console.log(binarysearch(arr, search, first, last));