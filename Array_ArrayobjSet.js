const arr1 = [2, 0, 3, 8, 25, 1, 30, 25];
const arr2 = [2, 26, 1, 30, 36];

const combinearray = [...arr1, ...arr2];
console.log(combinearray);
const uniquearray = [...new Set(combinearray)]
console.log(uniquearray.sort((a, b) => a > b ? 1 : -1));

const library = [
    {
        title: 'The Road Ahead',
        author: 'Bill Gates',
        libraryID: 1254
    },
    {
        title: 'Walter Isaacson',
        author: 'Steve Jobs',
        libraryID: 4264
    },
    {
        title: 'Mockingjay: The Final Book of The Hunger Games',
        author: 'Suzanne Collins',
        libraryID: 3245
    },
    {
        title: 'Mockingjay: The Final Book of The Hunger Games',
        author: 'Suzanne Collins',
        libraryID: 3245
    },
    {
        title: 'Mockingjay: The Final Book of The Hunger Games',
        author: 'Suzanne Collins',
        libraryID: 3249
    },
    {
        title: 'Test Isaacson',
        author: 'Steve Jobs',
        libraryID: 4262
    }];

//console.log(Array.from(new Set(library.map(JSON.stringify))).map(JSON.parse));
const uniqueValuesSet = new Set();
const filtr = library.filter((obj) => {
    const alredyexist = uniqueValuesSet.has(obj.libraryID);
    uniqueValuesSet.add(obj.libraryID);
    return !alredyexist;
});

console.log(filtr);