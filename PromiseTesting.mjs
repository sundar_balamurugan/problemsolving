import axios from "axios";

const Url = 'https://www.letsrevolutionizetesting.com/challenge.json';

function prom1(data) {
    axios.get(data)
        .then((res) => {
            const data = res.data;
            if (data.follow != undefined) {
                const newurl = data.follow.replace('?', '.json?')
                prom1(newurl);
                console.log(res.data)
            }
            else {
                console.log("Message:", data.message);
            }
        })
        .catch((err) => {
            console.log(err);
        })
}

prom1(Url);

