function add(a, b) {
    return new Promise(resolve => {
        setTimeout(() => { resolve(a + b) }, 1000);
    })

}

async function f() {
    const [a, b = 20] = [10];
    console.log('Output :', await add(a, b));
}

f();

// Decimal 
console.log(0.1 + 0.2);
console.log(0.1 + 0.2 == 0.3);