const Roman = {
    "I": 1,
    "V": 5,
    "X": 10,
    "L": 50,
    "C": 100,
    "D": 500,
    "M": 1000
}

const value = 'XLVIII'
const arr = value.split('')
console.log(arr);
let num = 0;
let curr;
for (i = 0; i < arr.length; i++) {
    curr = Roman[arr[i]]
    next = Roman[arr[i + 1]]
    if (curr < next) {
        num = num - curr;
    }
    else {
        num = num + curr;
    }

}
console.log(num)