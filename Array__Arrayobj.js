/*Array - 1: [2,0,3,8,25,1,30,25]
Array - 2: [2, 26, 1,30,36]*/

const arr1 = [2, 0, 3, 8, 25, 1, 30, 25];
const arr2 = [2, 26, 1, 30, 36];

const combinearray = arr1.concat(arr2);
let uniquearray = [];
for (let i of combinearray) {
    if (uniquearray.indexOf(i) === -1) {
        uniquearray.push(i);
    }
}
console.log(uniquearray.sort((a, b) => a > b ? 1 : -1));

//=======


const library = [
    {
        title: 'The Road Ahead',
        author: 'Bill Gates',
        libraryID: 1254
    },
    {
        title: 'Walter Isaacson',
        author: 'Steve Jobs',
        libraryID: 4264
    },
    {
        title: 'Mockingjay: The Final Book of The Hunger Games',
        author: 'Suzanne Collins',
        libraryID: 3245
    },
    {
        title: 'Mockingjay: The Final Book of The Hunger Games',
        author: 'Suzanne Collins',
        libraryID: 3245
    },
    {
        title: 'Mockingjay: The Final Book of The Hunger Games',
        author: 'Suzanne Collins',
        libraryID: 3249
    },
    {
        title: 'Test Isaacson',
        author: 'Steve Jobs',
        libraryID: 4262
    }];

let temp = {};
const filtbyid = library.filter((data) => {
    console.log(!temp[data.libraryID])
    return !temp[data.libraryID] && (temp[data.libraryID] = true)
})
console.log(filtbyid);
